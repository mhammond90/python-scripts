import click

"""Use Click to create a command-line tool that takes a name as
an argument and prints it if it does not begin with a P."""

@click.command()
@click.option('--name', default='Michael', help='Enter a name here.')

def greet(name):
    if name.startswith('P'):
        pass
    else:
        print(name)

if __name__ == '__main__':
    greet()